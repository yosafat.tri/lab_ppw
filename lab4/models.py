from django.db import models
from django.utils import timezone
from datetime import datetime

class Schedule(models.Model):
    day = models.CharField(max_length = 10)
    date = models.DateField()
    time = models.TimeField()
    activity = models.CharField(max_length = 40)
    place = models.CharField(max_length = 30)
    category = models.CharField(max_length = 15)

# Create your models here.
