from django.shortcuts import render,redirect
from .forms import Schedule_Form
from .models import Schedule

# Create your views here.

def firstdesign(request):
    response = {}
    return render(request, 'firstdesign.html',)

def biodata(request):
    response = {}
    return render(request, 'biodata.html',)

def form(request):
    response = {}
    return render(request, 'form.html',)

def jadwal(request):
    jadwal = Schedule.objects.all().values()
    response = {'jadwal' : jadwal}
    return render(request, 'jadwal.html', response)

def delete(request):
    jadwal = Schedule.objects.all().delete()
    return redirect("jadwal/")
    
def createPost(req):
    if req.method == 'POST':
        form = Schedule_Form(req.POST)
        if form.is_valid():
            schedule = Schedule()
            schedule.activity = form.cleaned_data['activity']
            schedule.place = form.cleaned_data['place']
            schedule.time = form.cleaned_data['time']
            schedule.day = form.cleaned_data['day']
            schedule.date = form.cleaned_data['date']
            schedule.category = form.cleaned_data['category']
            schedule.save()

        return redirect('jadwal/')


    else:
        form = Schedule_Form()
        return render(req, 'formjadwal.html', {'form' : form})