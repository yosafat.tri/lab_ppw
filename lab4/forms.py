from django import forms
class Schedule_Form(forms.Form):
    
    activity =  forms.CharField(label ='Activity' , max_length=40, widget=forms.TextInput(attrs={
        "class":"form-control"
    }))
    day = forms.CharField(label='Day',  max_length=10, widget=forms.TextInput(attrs={
        "class":"form-control"
    }))
    date = forms.DateField(label='Date' , widget=forms.DateInput(attrs={
        "class":"form-control"
    }))
    time = forms.TimeField(label='Time' , widget=forms.TimeInput(attrs={
        "class":"form-control"
    }))
    place = forms.CharField(label='Place' , max_length=30, widget=forms.TextInput(attrs={
        "class":"form-control"
    }))
    category = forms.CharField(label='Category' , max_length=15, widget=forms.TextInput(attrs={
        "class":"form-control"
    }))
