from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import landing, profile
from .models import Status
from .forms import Status_form
# Create your tests here.

class LandingPageTest(TestCase):
    def test_using_landing(self):
        found = resolve('/lab6/')
        self.assertEqual(found.func, landing)

    def test_using_template(self):
        response = Client().get('/lab6/')
        self.assertTemplateUsed(response, 'landing.html')

    def test_landing_message(self):
        response = Client().get('/lab6/')
        self.assertContains(response, 'Hello, ')
        self.assertContains(response, 'Apa kabar?')

    def test_landing_message2(self):
        response = Client().get('/lab6/')
        self.assertContains(response, 'What do you feel?')

class Url_test(TestCase):
    def url_can_be_accessed(self):
        response = Client().get('/lab6/')
        self.assertEqual(response.status_code, 200)

    def url_can_be_accessed2(self):
        response = Client().get('/lab6/profile/')
        self.assertEqual(response.status_code, 200)

class StatusModelTest(TestCase):
    def test_default_attribute(self):
        status = Status()
        self.assertEqual(status.post, '')

class ProfilePageTest(TestCase):
    def test_using_profile(self):
        found = resolve('/lab6/profile/')
        self.assertEqual(found.func, profile)

    def test_profile_using_template(self):
        response = Client().get('/lab6/profile/')
        self.assertTemplateUsed(response, 'profile.html')

    def test_profile_message(self):
        response = Client().get('/lab6/profile/')
        self.assertContains(response, "Hello, I'm Yosafat Tri Putra Brata")
        self.assertContains(response, 'This is my website')
        
    def test_profile_message2(self):
        response = Client().get('/lab6/profile/')
        self.assertContains(response, 'Name : Yosafat Tri Putra Brata')
        self.assertContains(response, 'NPM : 1706043430')
        self.assertContains(response, 'Description : ')

    def test_profile_message3(self):
        response = Client().get('/lab6/profile/')
        self.assertContains(response, 'Track Record :')
        self.assertContains(response, 'Graduated from :')
        self.assertContains(response, 'Currently studying at :')
        self.assertContains(response, 'Skills :')

    def test_profile_message4(self):
        response = Client().get('/lab6/profile/')
        self.assertContains(response, 'What I Like:')

    def test_profile_message5(self):
        response = Client().get('/lab6/profile/')
        self.assertContains(response, '(C)Copyright 2018 Yosafat Tri Putra Brata')
