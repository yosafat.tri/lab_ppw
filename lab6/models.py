from django.db import models
from django.utils import timezone
from datetime import datetime
# Create your models here.

class Status(models.Model):
    post = models.CharField(max_length = 300)
    time = models.TimeField(default = datetime.now())
