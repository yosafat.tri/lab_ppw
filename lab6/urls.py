from django.conf.urls import url
from .views import *

app_name = 'lab6'
urlpatterns = [
    url(r'^$', landing, name="landing"),
    url(r'^profile/', profile, name="profile"),
]
